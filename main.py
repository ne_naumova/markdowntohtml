from pathlib import Path
from tkinter import (END, EW, NSEW, SUNKEN, Label, Menu, Text, Tk, W,
                     filedialog, simpledialog)

import markdown
from tk_html_widgets import HTMLText

HELLO_MESSAGE = "Hello in MarkDown2HTML by Apple! (yeah, it's iPhone 11 color in left side)"


class MarkDownToHTML(Tk):
    def __init__(self):
        """Инициализация редактора, превью и менюшки.

        1.  Вначале мы инициализациируем наследуемый класс Tk (запуск страшнючих процессов, чтобы создать окно).
        2.  Потом мы создаем текстовый редактор с нужными нам параметрами,
            просим его обновлять превью при отпускании кнопки во время ввода и пихаем его в первую (левую) колонку.
        3.  Создаем превьюер, пихаем его во вторую (правую) колонку.
        4.  Инициализируем меню, статус бар, говорим, что по умолчанию у нас не выбран файл
        """
        super().__init__()

        self.editor = Text(background='#4e5851', foreground='white', insertbackground='white', font=('Arial', 20))
        self.editor.bind('<KeyRelease>', self.refresh_preview)
        self.editor.grid(row=0, column=0, sticky=NSEW)

        self.preview = HTMLText()
        self.preview.grid(row=0, column=1, sticky=NSEW)

        self.config(menu=MarkDownToHTMLMenuBar(self))
        self.statusbar = Label(self, text=HELLO_MESSAGE, bd=1, relief=SUNKEN, anchor=W)
        self.statusbar.grid(columnspan=2, sticky=EW)
        self.working_file = None

    def refresh_preview(self, event=None):
        """Главный "магический" метод нашей приложухи, который обновляет первью на основе HTML'а"""
        self.preview.set_html(self.get_html())

    def get_html(self):
        """Получаем HTML на основе текста из редактора"""
        return markdown.markdown(self.get_editor_text())

    def get_editor_text(self):
        """Получаем текст с первого по последний символ"""
        return self.editor.get(1.0, END)

    def create_new(self):
        """Создание нового документа. По сути, чистим едитор с перовго символа до конца, обновляем превью"""
        self.editor.delete(1.0, END)
        self.refresh_preview()

    def open_file(self):
        """Открытие файла.

        Вызываем диалог выбора файла с фильтром по текстовым файлам, чистим редактор с помощью create_new,
        вставяем в него содержимое файла, обновляем превью
        """
        target_file = filedialog.askopenfilename(title="Select file",
                                                 filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        if target_file:
            self.set_working_file(target_file)
            text = Path(self.working_file).read_text()
            self.create_new()
            self.editor.insert(1.0, text)
            self.refresh_preview()

    def set_working_file(self, target_file):
        """Устанавливает "рабочий" файл и пишет путь до него в статус бар"""
        self.working_file = target_file
        self.statusbar.config(text=f'Working file: {self.working_file}')

    def save_text(self):
        """Сохарнение. Если мы знаем файл, с которым работаем, пишем в него содержимое редактора,
        иначе просим выбрать файл через save_as
        """
        if self.working_file:
            Path(self.working_file).write_text(self.get_editor_text())
        else:
            self.save_text_as()

    def save_text_as(self):
        """Вызываем диалог выбора файла
        Если файл был выбран, записываем его в наш "редактируемый" и вызываем сохранение
        """
        target_file = filedialog.asksaveasfilename(title="Select file")
        if target_file:
            self.set_working_file(target_file)
            self.save_text()

    def export_to_html(self):
        """Экспорт в HTML, просим файл, куда хотим сохранить. Если был выбран, пишем в него html"""
        target_file = filedialog.asksaveasfilename(title="Select file", filetypes=[("html files", "*.html")])
        if target_file:
            Path(target_file).write_text(self.get_html())

    def show_all(self):
        """Показать обе панели, пихаем едитор в левую колонку, превью в правую"""
        self.editor.grid(column=0)
        self.preview.grid(column=1)

    def show_editor(self):
        """Показать едитор, пихаем его в левую колнку, убираем превеью"""
        self.editor.grid(column=0)
        self.preview.grid_remove()

    def show_preview(self):
        """Показать только превью, убираем едитор, помещаем превью в левую колонку"""
        self.editor.grid_remove()
        self.preview.grid(column=0)

    def add_link(self):
        """Добавить ссылку. Вызываем диалог, от нас просят ссылку.
        Если мы нажмём cancel, link будет пуст и в этом случае мы отменим добавление.

        В противном случае мы спрашиваем лейбел. Если диалог вернёт пустоту, посредством or'а мы возьмем саму ссылку.

        После в самый конец редактора пихаем нашу ссылку в MD-формате, обновляем preview
        """
        link = simpledialog.askstring("Add Link", "Enter Link")
        if link:
            label = simpledialog.askstring("Add Link", "Enter Label") or link
            self.editor.insert(END, f'[{label}]({link})')
            self.refresh_preview()

    def add_image(self):
        """Добавление картинки. Так же, как и ссылка, только вызываем
        диалог для выбора файла с фильтрацией расширения файла.
        """
        image = filedialog.askopenfilename(title="Select file",
                                           filetypes=(("image", "*.jpg"), ("all files", "*.*")))
        if image:
            self.editor.insert(END, f'![]({image})')
            self.refresh_preview()


class MarkDownToHTMLMenuBar(Menu):
    def __init__(self, master: MarkDownToHTML, **kw):
        """Инициализация меню-бара.

        Создаем инстанс меню, применяем его к нашему мастер-окну, создаем подменю "File", "Edit", "View"
        """
        super().__init__(master, **kw)
        self.master = master
        self.init_file_menu()
        self.init_edit_menu()
        self.init_view_menu()

    def init_file_menu(self):
        """Создание подменю "файл". Добавление команд и установка в качестве каскадоного для основного меню-бара"""
        file = Menu(self)
        file.add_command(label='New...', command=self.master.create_new)
        file.add_command(label='Open...', command=self.master.open_file)
        file.add_command(label='Save', command=self.master.save_text)
        file.add_command(label='Save As...', command=self.master.save_text_as)
        file.add_command(label='Export To HTML', command=self.master.export_to_html)
        self.add_cascade(label='File', menu=file)

    def init_edit_menu(self):
        """Создание Edit'а. Начинаем с создания подменю "Add", потом создаем меню "Edit",
        добавляем внутрь него "Add", юзаем "Edit" в качестве ещё одного каскадного на основном меню-баре.
                                                        - а больше ничего не юзаем (с)
        """
        add = Menu(self)
        add.add_command(label='Link...', command=self.master.add_link)
        add.add_command(label='Image...', command=self.master.add_image)

        edit = Menu(self)
        edit.add_cascade(label='Add...', menu=add)

        self.add_cascade(label='Edit', menu=edit)

    def init_view_menu(self):
        """Ну типа так же, как и у File"""
        view = Menu(self)
        view.add_command(label='Editor And Preview', command=self.master.show_all)
        view.add_command(label='Editor Markdown', command=self.master.show_editor)
        view.add_command(label='Preview', command=self.master.show_preview)
        self.add_cascade(label='View', menu=view)


if __name__ == '__main__':
    root = MarkDownToHTML()
    root.configure(background='gray')
    root.title("Markdown to html")
    root.geometry("1024x512+192+160")
    root.columnconfigure(0, weight=1)
    root.rowconfigure(0, weight=1)
    root.mainloop()
