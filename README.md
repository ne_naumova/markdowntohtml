Markdown To HTML
================
Translate your own markdown to html.

Installation
------------
```shell script
pip install Pillow
pip install -r requirements.txt
```

Running
-------
```
python main.py
```

Settings
--------
You can set font size for preview-mode via editing
```
tk_html_widgets / html_parser.py / Defs.FONT_SIZE
```

It's ugly way, bit library does not support this feature out of the box.
